from .frozen_scene import FrozenScene
from . import crosslight_swig as csl
from .crosslight_swig import RayBatch


class Intersector:
    def __init__(self, backend_name: str=None):
        """
        Creates a new intersector. Crosslight can provide multiple intersector backends. The parameter
        `backend_name` can be used to select which backend is used.
        """

        # Create the backend. The method returns None if the backend name is unrecognized.
        self._cpp_intersector = csl._create_intersector_by_name(backend_name)

        if self._cpp_intersector is None:
            raise ValueError(f'"{backend_name}" is not a valid backend name.')

    def create_batch(self, length: int):
        """ Creates a RayBatch instance that is optimized for use with this intersector """
        return csl._create_ray_batch(self._cpp_intersector, length)

    def find_intersections(
            self,
            scene: FrozenScene,
            batch: RayBatch,
            which = 'closest',
            n_rays = None):

        """
        Intersects the rays with the scene, finding the intersections. The batch is updated with the hit information.
        If `n_rays` is given and not None only the first `n_rays` rays are processed.

        For performance reasons intersecting may reorder the rays within the batch. Use the ray's ids if their order is
        important.

        The `which` parameter can be used to determine which intersections are found. By default (which='closest') the
        intersection closest to the ray's origin is found. If the exact point of intersection is not required the
        parameter can be set to 'any' instead, which may be faster. This will only test for whether the ray intersects
        the scene at all.

        TODO specify which values are exactly updated by this function. (Depends on whether closest or any is used!)
        """

        # The given scene has to be a FrozenScene, not a Scene. This is an easy
        # mistake to make, so protect against it.
        if not isinstance(scene, FrozenScene):
            raise TypeError(
                'Finding intersections requires a FrozenScene instance.')

        # TODO: Check that the ray batch was created by this intersector instance.
        #       This is nontrivial because multiple python objects may exist for the same C++ instance.

        if n_rays is None:
            n_rays = len(batch)

        if n_rays > len(batch):
            raise IndexError(n_rays)

        if which == 'closest':
            self._cpp_intersector._intersect_closest(
                scene._cpp_scene,
                batch,
                n_rays
            )

        elif which == 'any':
            self._cpp_intersector._intersect_any(
                scene._cpp_scene,
                batch,
                n_rays
            )

        else:
            raise ValueError(which)
