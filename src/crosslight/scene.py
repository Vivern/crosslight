import crosslight as cl
from . import crosslight_swig as clsw
from .intersector import Intersector
from .frozen_scene import FrozenScene
from typing import List
from .helpers import arg_as_buffer, arg_as_transform
import numpy as np


class _GeometryInScene:
    """
    Stores a geometry object together with metadata that is provided when adding it to the scene.
    """
    def __init__(self,
                 geometry: cl.Geometry,
                 transform: cl.Transform3f):

        self.geometry = geometry
        self.transform = transform


class Scene:
    def __init__(self):
        self._geometries: List[_GeometryInScene] = []

    def add(self,
            geometry: cl.Geometry,
            transform: cl.Transform3f = None):

        """
        Adds `geometry` to the scene, optionally applying a transformation.
        """

        if transform is None:
            transform = cl.Transform3f.identity()
        else:
            transform = arg_as_transform(transform)

        self._geometries.append(
            _GeometryInScene(
                geometry,
                transform))

    def freeze(self, intersector: Intersector) -> FrozenScene:
        # Create the result object
        cpp_result = clsw._create_scene(intersector._cpp_intersector)

        # Add the geometries
        for scene_geom in self._geometries:
            geom = scene_geom.geometry

            # create a CPP geometry from the python object
            positions_buf = arg_as_buffer(
                geom.positions,
                dtype=np.float32,
                ndims=3
            )

            faces_buf = arg_as_buffer(
                geom.faces,
                dtype=np.int32,
                ndims=3
            )

            cpp_geom = clsw._create_trianglemesh(positions_buf, faces_buf)

            # Add it
            cpp_result.addGeometry(
                cpp_geom,
                arg_as_transform(scene_geom.transform))

        # Finalize and return
        cpp_result.commit()
        return FrozenScene(cpp_result)

    def __delitem__(self, geom: cl.Geometry):
        """ Removes the geometry from the scene. Raises a `KeyError` if it isn't contained in the scene. """

        for ii, obj in enumerate(self._geometries):
            if obj.geometry is geom:
                del self._geometries[ii]
                return

        raise KeyError(geom)

    def __contains__(self, item: cl.Geometry):
        return any(
            g.geometry is item for g in self._geometries
        )

    def __len__(self):
        return len(self._geometries)

