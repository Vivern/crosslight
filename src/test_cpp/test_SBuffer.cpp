#include <gtest/gtest.h>
#include "../core/SBuffer.hpp"


using namespace cl;


TEST (TestSBuffer, testShape) {
    // Instantiate a new buffer, with both static and dynamic shapes. Test that the buffer's runtime shape is correct.

    const Matrix<int, 3, 1> dynShape(2, 5, 6);
    auto buf = SBuffer<int, 1, DYN, 3, 4, DYN, DYN, 7>::create<3>( dynShape );

    ASSERT_EQ(buf.shape(0), 1);
    ASSERT_EQ(buf.shape(1), 2);
    ASSERT_EQ(buf.shape(2), 3);
    ASSERT_EQ(buf.shape(3), 4);
    ASSERT_EQ(buf.shape(4), 5);
    ASSERT_EQ(buf.shape(5), 6);
    ASSERT_EQ(buf.shape(6), 7);
}


TEST (TestSBuffer, testStrides) {
    // Instantiate a new buffer, with both static and dynamic shapes. Test that the buffer's runtime strides are correct.

    const Matrix<int, 2, 1> dynShape(2, 3);
    auto buf = SBuffer<uint8_t, 1, DYN, DYN, 4>::create<2>( dynShape );

    ASSERT_EQ(buf.stride(0), 24);
    ASSERT_EQ(buf.stride(1), 12);
    ASSERT_EQ(buf.stride(2), 4);
    ASSERT_EQ(buf.stride(3), 1);
}


TEST (TestSBuffer, testBufferAsImage) {
    // Set each pixel in a 3D (height/width/channels) buffer

    const int HEIGHT=512, WIDTH=1024, CHANNELS=3;

    auto buf = SBuffer<int, HEIGHT, WIDTH, CHANNELS>::create<0>( Matrix<int, 0, 1>() );
    memset(&buf.get(0), 1, HEIGHT * WIDTH * CHANNELS * sizeof(int));

    // use per component indexing to verify the values and set new ones
    for (int s0=0; s0 < HEIGHT; ++s0) {
        for (int s1 = 0; s1 < HEIGHT; ++s1) {
            for (int s2 = 0; s2 < CHANNELS; ++s2) {
                int &cur = buf.get(s0, s1, s2);
                ASSERT_EQ(cur, 0x01010101);
                cur = s0 * 1000000 + s1 * 1000 + s2;
            }
        }
    }
}
