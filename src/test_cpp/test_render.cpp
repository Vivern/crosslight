#include <gtest/gtest.h>
#include <memory>
#include <fancy-assert>
#include <string>
#include <Eigen/Dense>
#include <algorithm>
#include "../core/crosslight.hpp"
#include "../core/embree/EmbreeIntersector.hpp"


using namespace Eigen;
using namespace cl;


// scene geometry
const float cubeVertexPositions[] = {
        0.0, 0.0, 0.0,
        0.0, 0.0, 1.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 1.0,
        1.0, 0.0, 0.0,
        1.0, 0.0, 1.0,
        1.0, 1.0, 0.0,
        1.0, 1.0, 1.0,
};
const unsigned int cubeNVertices = sizeof(cubeVertexPositions) / sizeof(float) / 3;


const uint32_t cubeFaceIndices[] = {
        // Back
        0, 2, 4,
        2, 6, 4,

        // Front
        1, 3, 5,
        3, 7, 5,

        // Left
        0, 3, 1,
        0, 2, 3,

        // Right
        4, 7, 5,
        4, 6, 7,

        // Bottom
        0, 5, 4,
        0, 1, 5,

        // Top
        2, 7, 6,
        2, 3, 7,
};
const unsigned int cubeNFaces = sizeof(cubeFaceIndices) / sizeof(uint32_t) / 3;




void write_ppm(
        const char *filename,
        int width,
        int height,
        const char *comment,
        const uint8_t *pixels) {

    const int saturationValue = 255;
    FILE *fd = fopen(filename, "wb");

    // write the header
    fprintf(fd, "P6\n # %s\n %d\n %d\n %d\n", comment, width, height,
            saturationValue);

    // write the pixel data
    fwrite(pixels, width * height * 3, 1, fd);

    // cleanup
    fclose(fd);
}

std::shared_ptr<TriangleMesh> createCube() {
    // Create the buffers
    const Matrix<int, 1, 1> positionsShape( cubeNVertices );
    auto positions = SBufferX3f::create<1>(positionsShape);

    for (int ii=0; ii<cubeNVertices; ++ii) {
        positions.get(ii, 0) = cubeVertexPositions[3 * ii + 0];
        positions.get(ii, 1) = cubeVertexPositions[3 * ii + 1];
        positions.get(ii, 2) = cubeVertexPositions[3 * ii + 2];
    }

    const Matrix<int, 1, 1> facesShape( cubeNFaces );
    auto faces = SBufferX3i32::create<1>(facesShape);

    for (int ii=0; ii<cubeNFaces; ++ii) {
        faces.get(ii, 0) = cubeFaceIndices[3 * ii + 0];
        faces.get(ii, 1) = cubeFaceIndices[3 * ii + 1];
        faces.get(ii, 2) = cubeFaceIndices[3 * ii + 2];
    }

    // Construct the cube
    auto result = std::make_shared<TriangleMesh>(positions, faces);
    return result;
}


TEST (TestRender, testRender) {
    // Renders a very simple scene and ensure that the intersections match the expectations. The scene contains a cube
    // from (0, 0, 0) to (1, 1, 1). The camera is orthographic and is set such that three quadrants of the result should
    // be empty while the top right one contains the cube.

    // Image setup
    // The image is RGB
    const int width = 16, height = 8;
    auto resultBuffer = SBuffer<uint8_t, height, width, 3>::create<0>( Matrix<int, 0, 1>() );
    memset(&resultBuffer.get(0), 0, width * height * 3);

    // Create the intersector
    auto intersector = std::make_shared<EmbreeIntersector>();

    // Set up the scene
    auto scene = intersector->createScene(intersector);

    auto cubeGeom = createCube();
    scene->addGeometry(cubeGeom, Transform3f::Identity());

    scene->commit();

    // Set up the ray batch
    auto rayBatch = intersector->createBatch(intersector, width * height);

    const float cam1 = -1.0f, cam2 = 1.0f;

    SBufferXu  &rayIdBuffer  = rayBatch->getRayIdBuffer();
    SBufferX3f &originBuffer = rayBatch->getOriginBuffer();
    SBufferX3f &dirBuffer    = rayBatch->getDirBuffer();
    SBufferXf  &lengthBuffer = rayBatch->getLengthBuffer();

    for (int ii=0; ii<width*height; ++ii) {
        const int xx = ii % width;
        const int yy = ii / width;

        const float xfac = xx / static_cast<float>(width);
        const float yfac = yy / static_cast<float>(height);

        // Ray ids
        rayIdBuffer.get(ii) = ii;

        // Origins
        originBuffer.get(ii, 0) = cam1 + xfac * (cam2 - cam1);
        originBuffer.get(ii, 1) = cam1 + yfac * (cam2 - cam1);
        originBuffer.get(ii, 2) = -10;

        // Directions
        dirBuffer.get(ii, 0) = 0;
        dirBuffer.get(ii, 1) = 0;
        dirBuffer.get(ii, 2) = 1;

        // Lengths
        lengthBuffer.get(ii) = 10000;
    }

    // Find the intersections
    intersector->intersectClosest(*scene, *rayBatch, rayBatch->size());

    // Create an image from the intersections
    //   &
    // Verify the result
    for (int jj=0; jj<width*height; ++jj) {
        const int ii = rayIdBuffer.get(jj);
        const int xx = ii % width;
        const int yy = ii / width;
        // The y coordinate is flipped in images, because the coordinate system is on the bottom left. This is the
        // y coordinate in image space.
        const int imyy = height - 1 - yy;

        // Convert the hit to a visible color
        float length = lengthBuffer.get(ii);
        float resGray = length / 50;

        // Verify the result
        bool shouldHit = xx >= (width / 2) && yy >= (height / 2);
        if (shouldHit) {
            ASSERT_NEAR(length, 10.0f, 0.5f);
        } else {
            ASSERT_NEAR(length, 10000.0f, 0.5f);
        }

        // Write the result into the image
        resGray = std::max(resGray, 0.0f);
        resGray = std::min(resGray, 1.0f);
        uint8_t resInt = static_cast<uint8_t>(resGray * 255);
        resultBuffer.get(imyy, xx, 0) = resInt;
        resultBuffer.get(imyy, xx, 1) = resInt;
        resultBuffer.get(imyy, xx, 2) = resInt;
    }

    // Optional: Write out the result as image
//    write_ppm("out.ppm",
//            resultBuffer.shape(1),
//            resultBuffer.shape(0),
//            "final output",
//            &resultBuffer.get(0));
}

