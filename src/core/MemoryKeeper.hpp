#ifndef CROSSLIGHT_MEMORYKEEPER_H
#define CROSSLIGHT_MEMORYKEEPER_H


#include <functional>


namespace cl {

    class MemoryKeeper final {
    public:
        MemoryKeeper();

        explicit MemoryKeeper(size_t nBytes);

        MemoryKeeper(void *memory, const std::function<void(void *)> &destroyCallback);

        MemoryKeeper(const MemoryKeeper &other) = delete;
        MemoryKeeper(MemoryKeeper &&other) = delete;

        ~MemoryKeeper();

        explicit operator bool() const;

        void *getMemory() const;

        void *getMemory();

    private:
        void * const memory;
        std::function<void (void *)> destroyCallback;
    };

}


#endif  // CROSSLIGHT_MEMORYKEEPER_H
