#include "EmbreeIntersector.hpp"
#include <fancy-assert>
#include "EmbreeScene.hpp"
#include "EmbreeBatch.hpp"

static void embreeErrorCallback(void *userPtr, RTCError code, const char *str) {
    std::stringstream message;

    message << "RTC_ERROR_";

    switch (code) {
        case RTC_ERROR_UNKNOWN:            message << "UNKNOWN"; break;
        case RTC_ERROR_INVALID_ARGUMENT:   message << "INVALID_ARGUMENT"; break;
        case RTC_ERROR_INVALID_OPERATION:  message << "INVALID_OPERATION"; break;
        case RTC_ERROR_OUT_OF_MEMORY:      message << "OUT_OF_MEMORY"; break;
        case RTC_ERROR_UNSUPPORTED_CPU:    message << "UNSUPPORTED_CPU"; break;
        case RTC_ERROR_CANCELLED:          message << "CANCELLED"; break;

        default:
            assertNotReached();
    }

    message << ": " << str << std::endl;
    throw std::runtime_error(message.str());
}


namespace cl {

    EmbreeIntersector::EmbreeIntersector(const std::string &embreeConfig)
        : Intersector(),
          embDevice(rtcNewDevice(embreeConfig.c_str())) {

        rtcSetDeviceErrorFunction(embDevice, embreeErrorCallback, nullptr);
    }

    EmbreeIntersector::~EmbreeIntersector() {

        rtcReleaseDevice(embDevice);
    }

    std::shared_ptr<Scene> EmbreeIntersector::createScene(const std::shared_ptr<Intersector> &thisShared) {
        assertEq(thisShared.get(), this);

        // cannot use make_shared because the constructor is private
        EmbreeScene *result = new EmbreeScene( std::static_pointer_cast<EmbreeIntersector>(thisShared) );
        return std::shared_ptr<EmbreeScene>(result);
    }

    int EmbreeIntersector::getPreferredBatchSize() {
        // TODO: Run some tests to get the best number.
        //       The current value is only a guess.
        return 1 << 14;
    }

    std::shared_ptr<RayBatch> EmbreeIntersector::createBatch(const std::shared_ptr<Intersector> &thisShared,
                                                             int size) {
        assertEq(thisShared.get(), this);

        // cannot use make_shared because the constructor is private
        EmbreeBatch *result = new EmbreeBatch( std::static_pointer_cast<EmbreeIntersector>(thisShared), size );
        return std::shared_ptr<EmbreeBatch>(result);
    }

    RTCDevice &EmbreeIntersector::getEmbreeDevice() {
        return embDevice;
    }

    void EmbreeIntersector::doIntersectClosest(
        const Scene &scene,
        RayBatch &batch,
        int upTo) {

        assertLe(upTo, batch.size());

        // cast to backend-specific types
        auto &castScene = assert_cast<const EmbreeScene&>(scene);
        auto &castBatch = assert_cast<EmbreeBatch&>(batch);

        // make sure the given parameters use the same intersector
        assertEq(castScene.getIntersector().get(), this);
        assertEq(castBatch.getIntersector().get(), this);

        // Clear the batch, making it ready for intersection
        castBatch.clear();

        RTCScene embScene = castScene.getEmbreeScene();

        RTCIntersectContext context;
        rtcInitIntersectContext(&context);

        SBuffer<RTCRayHit, DYN> embRayHits = castBatch.getEmbreeRayHits();

        // intersect
        rtcIntersect1M(
            embScene,
            &context,
            &embRayHits.get(0),
            static_cast<unsigned int>(upTo),
            embRayHits.stride(0));

        // Mark the batch as dirty
        castBatch.cleared = false;
    }

    void EmbreeIntersector::doIntersectAny(
        const Scene &scene,
        RayBatch &batch,
        int upTo) {

        assertLe(upTo, batch.size());

        // cast to backend-specific types
        auto &castScene = assert_cast<const EmbreeScene&>(scene);
        auto &castBatch = assert_cast<EmbreeBatch&>(batch);

        // make sure the given parameters use the same intersector
        assertEq(castScene.getIntersector().get(), this);
        assertEq(castBatch.getIntersector().get(), this);

        // Clear the batch, making it ready for intersection
        castBatch.clear();

        RTCScene embScene = castScene.getEmbreeScene();

        RTCIntersectContext context;
        rtcInitIntersectContext(&context);

        SBuffer<RTCRayHit, DYN> embRayHits = castBatch.getEmbreeRayHits();

        // intersect
        rtcOccluded1M(
                embScene,
                &context,
                &embRayHits.get(0).ray,
                static_cast<unsigned int>(upTo),
                embRayHits.stride(0));

        // Mark the batch as dirty
        castBatch.cleared = false;
    }

}
