#include "TriangleMesh.hpp"


namespace cl {

    TriangleMesh::TriangleMesh(const SBufferX3f &positions,
                               const SBufferX3i32 &faces)
         : positions(positions),
           faces(faces) {}

    int TriangleMesh::nVertices() const {
        return positions.shape(0);
    }

    int TriangleMesh::nFaces() const {
        return faces.shape(0);
    }

    const SBufferX3f &TriangleMesh::getPositions() const {
        return positions;
    }

    const SBufferX3i32 &TriangleMesh::getFaces() const {
        return faces;
    }
}
