#include "MemoryKeeper.hpp"


namespace cl {
    MemoryKeeper::MemoryKeeper()
        : memory(nullptr),
          destroyCallback(nullptr) {}

    MemoryKeeper::MemoryKeeper(size_t nBytes)
        : memory( new uint8_t[nBytes] ),
          destroyCallback( [](void *buf) { delete[] static_cast<uint8_t*>(buf); } ) {}

    MemoryKeeper::MemoryKeeper(void *memory, const std::function<void(void *)> &destroyCallback)
            : memory(memory), destroyCallback(destroyCallback) {}

    MemoryKeeper::~MemoryKeeper() {

        if (memory && destroyCallback) {
            destroyCallback(memory);
        }
    }

    MemoryKeeper::operator bool() const {
        return memory != nullptr;
    }

    void *MemoryKeeper::getMemory() const {
        return memory;
    }

    void *MemoryKeeper::getMemory() {
        return memory;
    }
}
