#include "Buffer.hpp"


namespace cl {

    std::shared_ptr<Buffer> Buffer::createNew(int order, const int *shape, NPY_TYPES dataType) {
        assertGt(order, 0);
        assertLe(order, MAX_BUFFER_ORDER);

        // determine the total number of elements
        int numElements = 1;
        for (int ii=0; ii<order; ++ii) {
            numElements *= shape[ii];
        }

        // allocate the memory
        type_info typeInfo(dataType);
        auto memory = std::make_shared<MemoryKeeper>(numElements * typeInfo.size);

        // calculate the strides
        int strides[MAX_BUFFER_ORDER];
        strides[order - 1] = typeInfo.size;
        for (int ii=order-2; ii>=0; --ii) {
            strides[ii] = shape[ii + 1] * strides[ii + 1];
        }

        return createNew(
                memory,
                reinterpret_cast<uint8_t *>(memory->getMemory()),
                dataType,
                order,
                shape,
                strides);
    }

    std::shared_ptr<Buffer> Buffer::createNew(
            const std::shared_ptr<MemoryKeeper> &memory,
            void *first,
            NPY_TYPES dType,
            int order,
            const int *sizes,
            const int *strides) {

        assertGt(order, 0);
        assertLe(order, MAX_BUFFER_ORDER);

        auto result = std::shared_ptr<Buffer>(
                new Buffer(memory, first, dType, order, sizes, strides));

        result->weakThis = result;
        return result;
    }

    const std::shared_ptr<MemoryKeeper> &Buffer::memory() const {
        return memory_;
    }

    NPY_TYPES Buffer::dType() const {
        return dType_;
    }

    int Buffer::order() const {
        return order_;
    }

    const int *Buffer::shape() const {
        return shape_;
    }

    int Buffer::shape(int dim) const {
        assertGe(dim, 0);
        assertLt(dim, order());

        return shape_[dim];
    }

    const int *Buffer::strides() const {
        return strides_;
    }

    int Buffer::stride(int dim) const {
        assertGe(dim, 0);
        assertLt(dim, order());

        return strides_[dim];
    }

    void *Buffer::first() const {
        return first_;
    }

    void *Buffer::first() {
        return first_;
    }

    const void *Buffer::getVoid(int nIndices, const int *indices) const {
        assertLe(nIndices, order());

        uint8_t *result = first_;

        for (int ii=0; ii<nIndices; ++ii) {
            int index = indices[ii];
            assertGe(index, 0);
            assertLt(index, shape_[ii]);

            result += index * strides_[ii];
        }

        return result;
    }

    void *Buffer::getVoid(int nIndices, const int *indices) {
        const auto *constThis = this;
        return const_cast<void *>( constThis->getVoid(nIndices, indices) );
    }

    std::shared_ptr<Buffer> Buffer::subBuffer(const int *firstIndex, const int *subShape) const {

        for (int ii=0; ii<order(); ++ii) {
            assertGe(firstIndex[ii], 0);
            assertLt(firstIndex[ii], shape(ii));
            assertLe(firstIndex[ii] + subShape[ii], shape(ii));
        }

        return createNew(
                memory_,
                (uint8_t *)( getVoid(order(), firstIndex) ),
                dType(),
                order(),
                subShape,
                strides_);
    }

    Buffer::Buffer(const std::shared_ptr<MemoryKeeper> &memory,
                   void *first,
                   NPY_TYPES dType,
                   int order,
                   const int *sizes,
                   const int *strides)
            : memory_(memory),
              first_(static_cast<uint8_t *const>(first)),
              dType_(dType),
              order_(order) {

        assert(*memory);
        assertGt(order, 0);
        assertLe(order, MAX_BUFFER_ORDER);

        for (int ii=0; ii<order; ++ii) {
            this->shape_[ii]   = sizes[ii];
            this->strides_[ii] = strides[ii];

            assertGe(shape_[ii], 0);
            assertGe(strides_[ii], 0);
        }
    }
}
