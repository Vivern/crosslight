#ifndef CROSSLIGHT_CORE_TYPE_HELPERS_H_
#define CROSSLIGHT_CORE_TYPE_HELPERS_H_


#include <cstddef>
#include <fancy-assert>

// Disable the deprecated API
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/ndarraytypes.h>


namespace cl_internal {

    // This struct is used to determine the alignment of datatypes (as defined by Numpy). This is done in the
    // constructor of `type_info`. The struct is defined here because C++ doesn't allow for inline struct definitions in
    // constructors.
    template<typename T>
    struct determineOffsetStruct {
        char a;
        T b;
    };

};


namespace cl {

    /**
     * @brief Struct containing information relating to in memory storage of datatypes
     */
    struct type_info {
        /**
         * @brief Creates an uninitialized structure
         */
        type_info() = default;

        /**
         * @brief Explicitly initializes the structure with the given arguments
         */
        type_info(size_t size, size_t alignment);

        /**
         * @brief Creates a new structure reflecting the storage of the given datatype.
         *
         * Use the static `forType` template if the datatype is known at compile time.
         */
        explicit type_info(NPY_TYPES npy_type_num);

        /**
         * @brief Creates a new structure reflecting the storage of the given datatype.
         *
         * A non-template constructor is available should the type not be known at compile time.
         */
        template<typename T>
        static type_info forType() {
            return {
                    sizeof(T),
                    offsetof(cl_internal::determineOffsetStruct<T>, b)
            };
        }

        /**
         * @brief The data type's size, as returned by `sizeof`
         */
        size_t size;

        /**
         * @brief The data type's alignment, as defined by Numpy.
         *
         * See https://docs.scipy.org/doc/numpy-1.13.0/reference/c-api.types-and-structures.html#c.PyArray_Descr.alignment
         */
        size_t alignment;
    };

    /**
     * @brief Get the correspondig C datatype for numpy's types.
     *
     * Returns a class containing information for the given numpy type. The C type can be accessed as
     * `npTypeToC<NPY_SOME_TYPE>::type`.
     */
    template<NPY_TYPES T> class npTypeToC   {                                };
    template<> class npTypeToC<NPY_INT8>    { public: typedef int8_t   type; };
    template<> class npTypeToC<NPY_INT16>   { public: typedef int16_t  type; };
    template<> class npTypeToC<NPY_INT32>   { public: typedef int32_t  type; };
    template<> class npTypeToC<NPY_INT64>   { public: typedef int64_t  type; };
    template<> class npTypeToC<NPY_UINT8>   { public: typedef uint8_t  type; };
    template<> class npTypeToC<NPY_UINT16>  { public: typedef uint16_t type; };
    template<> class npTypeToC<NPY_UINT32>  { public: typedef uint32_t type; };
    template<> class npTypeToC<NPY_UINT64>  { public: typedef uint64_t type; };
//    template<> class npTypeToC<NPY_FLOAT16> { public: typedef half     type; };
    template<> class npTypeToC<NPY_FLOAT32> { public: typedef float    type; };
    template<> class npTypeToC<NPY_FLOAT64> { public: typedef double   type; };

    template<typename T> NPY_TYPES cTypeToNp() { assertNotReached(); }
    template<> NPY_TYPES cTypeToNp<int8_t  >();
    template<> NPY_TYPES cTypeToNp<int16_t >();
    template<> NPY_TYPES cTypeToNp<int32_t >();
    template<> NPY_TYPES cTypeToNp<int64_t >();
    template<> NPY_TYPES cTypeToNp<uint8_t >();
    template<> NPY_TYPES cTypeToNp<uint16_t>();
    template<> NPY_TYPES cTypeToNp<uint32_t>();
    template<> NPY_TYPES cTypeToNp<uint64_t>();
//    template<> NPY_TYPES cTypeToNp<half    >();
    template<> NPY_TYPES cTypeToNp<float   >();
    template<> NPY_TYPES cTypeToNp<double  >();
}


#endif  // CROSSLIGHT_CORE_TYPE_HELPERS_H_
