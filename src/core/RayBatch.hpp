#ifndef CROSSLIGHT_CORE_RAYBATCH_H_
#define CROSSLIGHT_CORE_RAYBATCH_H_


#include <Eigen/Dense>
#include <memory>
#include "SBuffer.hpp"


using namespace Eigen;


namespace cl {
    class Intersector;

    class RayBatch {
    public:
        virtual ~RayBatch() = default;

        int size() const;

        std::shared_ptr<Intersector> getIntersector() const;

        SBufferXu  &getRayIdBuffer();
        SBufferX3f &getOriginBuffer();
        SBufferX3f &getDirBuffer();
        SBufferXf  &getLengthBuffer();
        SBufferXu  &getGeometryIdBuffer();
        SBufferXu  &getPrimitiveIdBuffer();

    protected:
        RayBatch(const std::shared_ptr<Intersector> &intersector, int size);

        void initBuffers(
                const SBufferXu  &rayIdBuffer,
                const SBufferX3f &originBuffer,
                const SBufferX3f &dirBuffer,
                const SBufferXf  &lengthBuffer,
                const SBufferXu  &geometryIdBuffer,
                const SBufferXu  &primitiveIdBuffer);

    private:
        const std::shared_ptr<Intersector> intersector;
        const int size_;

        SBufferXu   rayIdBuffer;
        SBufferX3f  originBuffer;
        SBufferX3f  dirBuffer;
        SBufferXf   lengthBuffer;
        SBufferXu   geometryIdBuffer;
        SBufferXu   primitiveIdBuffer;
    };
}

#endif  // CROSSLIGHT_CORE_RAYBATCH_H_
