#ifndef CROSSLIGHT_CORE_SBUFFER_H_
#define CROSSLIGHT_CORE_SBUFFER_H_


#include <memory>
#include <functional>
#include <Eigen/Dense>
#include <algorithm>
#include "type_helpers.hpp"
#include "MemoryKeeper.hpp"
#include "Buffer.hpp"


using namespace Eigen;


namespace cl {
    const int DYN = -1;

    template<typename T, int ... STATICSHAPE>
    class SBuffer final {
    private:

        /**
         * Initializes the buffer's `shape` attribute.
         */
        template<int N_DYNAMIC_DIMS>
        static void initShape(Matrix<int, N_DYNAMIC_DIMS, 1> dynShape, int *shapeOut) {

            const int staticShape[] = { STATICSHAPE... };

            // Make sure the correct number of dynamic sizes was passed.
            const int n_dyn_dimensions_in_staticshape = std::count(&staticShape[0], &staticShape[order()], DYN);
            assertEq(N_DYNAMIC_DIMS, n_dyn_dimensions_in_staticshape);

            // Determine and store the buffer's runtime shape
            for (int statii=0, dynii=0; statii < order(); ++statii) {

                if (staticShape[statii] == DYN) {
                    shapeOut[statii] = dynShape[dynii];
                    ++dynii;
                } else {
                    shapeOut[statii] = staticShape[statii];
                }

                assertGt(shapeOut[statii], 0);
            }
        }

        /**
         * Initializes the buffer's strides to dense values.
         */
        static void initDenseStrides(int *stridesOut) {
            stridesOut[order() - 1] = sizeof(T);

            for (int ii=order()-2; ii>=0; --ii) {
                stridesOut[ii] = stridesOut[ii + 1] * stridesOut[ii + 1];
            }
        }


    public:
        SBuffer() = default;

        /**
         * @brief Constructs a new SBuffer, automatically taking care of memory allocation.
         *
         * The buffer's memory is contiguous.
         *
         * This is not a constructor because there is no way to pass template parameters to them and Visual Studio
         * fails even for simple template matching.
         */
        template<int N_DYNAMIC_DIMS>
        static SBuffer<T, STATICSHAPE...> create(
                Matrix<int, N_DYNAMIC_DIMS, 1> dynShape) {

            // Create the result
            SBuffer<T, STATICSHAPE...> result;

            // Initialize the shape & strides
            initShape<N_DYNAMIC_DIMS>(dynShape, result.shape_);
            initDenseStrides(result.strides_);

            // determine the total number of elements
            int numElements = 1;
            for (int ii=0; ii<order(); ++ii) {
                numElements *= result.strides_[ii];
            }

            // Allocate memory
            result.memory_ = std::make_shared<MemoryKeeper>(
                    numElements * sizeof(T)
            );
            result.first_ = static_cast<uint8_t *>( result.memory_->getMemory() );

            return result;
        }

        /**
         * @brief Constructs a new SBuffer with most parameters explicitly specified.
         *
         * This is not a constructor because there is no way to pass template parameters to them and Visual Studio
         * fails even for simple template matching.
         */
        template<int N_DYNAMIC_DIMS>
        static SBuffer<T, STATICSHAPE...> create(
                const std::shared_ptr<MemoryKeeper> &memory,
                void *first,
                Matrix<int, N_DYNAMIC_DIMS, 1> dynShape,
                Matrix<int, sizeof...(STATICSHAPE), 1> strides) {

            assert(*memory);
            assertNotNull(first);

            // Create the result
            SBuffer<T, STATICSHAPE...> result;

            // Initialize the memory
            result.memory_ = memory;
            result.first_ = static_cast<uint8_t *>(first);

            // Determine the runtime shape
            initShape<N_DYNAMIC_DIMS>(dynShape, result.shape_);

            // Copy the strides
            std::copy(&strides[0], &strides[0] + order(), &result.strides_[0]);

            return result;
        }

        SBuffer(const SBuffer &) = default;

        SBuffer(SBuffer &&) noexcept = default;

        SBuffer(const Buffer &other)
            : memory_(other.memory()),
              first_(static_cast<uint8_t *>(other.first())) {

            const int staticShape[] = { STATICSHAPE... };

            // Check that the copy makes sense
            // Same order
            assertEq(order(), other.order());

            // The dynamic buffer matches this buffer's static shape
            for (int ii=0; ii<order(); ++ii) {
                if (staticShape[ii] == DYN)
                    continue;

                assertEq(staticShape[ii], other.shape(ii));
            }

            // Same datatype
            assertEq(dType(), other.dType());


            // Copy the shape
            std::copy(other.shape(), other.shape() + order(), &shape_[0]);

            // Copy the strides
            std::copy(other.strides(), other.strides() + order(), &strides_[0]);
        }

        SBuffer& operator=(const SBuffer &rhs) = default;

        ~SBuffer() = default;

        const std::shared_ptr<MemoryKeeper> &memory() const {
            return memory_;
        }

        static int order() {
            return sizeof...(STATICSHAPE);
        }

        static NPY_TYPES dType() {
            return cTypeToNp<T>();
        }

        const int *shape() const {
            return shape_;
        }

        int shape(int dim) const {
            assertGe(dim, 0);
            assertLt(dim, order());

            return shape_[dim];
        }

        const int *strides() const {
            return strides_;
        }

        int stride(int dim) const {
            assertGe(dim, 0);
            assertLt(dim, order());

            return strides_[dim];
        }

        const T *first() const {
            return reinterpret_cast<const T *>(first_);
        }

        T *first() {
            return reinterpret_cast<T *>(first_);
        }

        const T &get(int i0) const {
            assertLt(i0, shape(0));

            const uint8_t *result = first_ +  \
                strides_[0] * i0;

            return reinterpret_cast<const T &>(*result);
        }

        T &get(int i0) {
            assertLt(i0, shape(0));

            uint8_t *result = first_ +        \
                strides_[0] * i0;

            return reinterpret_cast<T &>(*result);
        }

        const T &get(int i0, int i1) const {
            assertGe(order(), 2);
            assertLt(i0, shape(0));
            assertLt(i1, shape(1));

            const uint8_t *result = first_ +  \
                strides_[0] * i0 +            \
                strides_[1] * i1;

            return reinterpret_cast<const T &>(*result);
        }

        T &get(int i0, int i1) {
            assertGe(order(), 2);
            assertLt(i0, shape(0));
            assertLt(i1, shape(1));

            uint8_t *result = first_ +        \
                strides_[0] * i0 +            \
                strides_[1] * i1;
            return reinterpret_cast<T &>(*result);
        }

        const T &get(int i0, int i1, int i2) const {
            assertGe(order(), 3);
            assertLt(i0, shape(0));
            assertLt(i1, shape(1));
            assertLt(i2, shape(2));

            const uint8_t *result = first_ +  \
                strides_[0] * i0 +            \
                strides_[1] * i1 +            \
                strides_[2] * i2;
            return reinterpret_cast<const T &>(*result);
        }

        T &get(int i0, int i1, int i2) {
            assertGe(order(), 3);
            assertLt(i0, shape(0));
            assertLt(i1, shape(1));
            assertLt(i2, shape(2));

            uint8_t *result = first_ +        \
                strides_[0] * i0 +            \
                strides_[1] * i1 +            \
                strides_[2] * i2;
            return reinterpret_cast<T &>(*result);
        }

        template<int INDEX_DIMS>
        T &get(const Matrix<int, INDEX_DIMS, 1> &index) {
            int offset = 0;
            for (int ii=0; ii<INDEX_DIMS; ++ii) {
                offset += strides_[ii] * index[ii];
            }

            uint8_t *result = first_ + offset;
            return reinterpret_cast<T &>(*result);
        }

    private:
        std::shared_ptr<MemoryKeeper> memory_;
        uint8_t *first_;
        int shape_[ sizeof...(STATICSHAPE) ];
        int strides_[ sizeof...(STATICSHAPE) ];
    };

    template<typename T, int ... DIMS>
    std::ostream &operator<<(std::ostream &os, const SBuffer<T, DIMS...> &buf);

    template<typename T, int S0, int S1>
    std::ostream &operator<<(std::ostream &os, const SBuffer<T, S0, S1> &buf) {

        for (int i0=0; i0<buf.shape(0); ++i0) {
            if (i0 == 0)
                os << "[[";
            else
                os << " [";

            for (int i1=0; i1<buf.shape(1); ++i1) {
                if (i1 != 0)
                    os << ", ";

                os << buf.get(i0, i1);
            }

            if (i0 == (buf.shape(0) - 1))
                os << "]]\n";
            else
                os << "],\n";
        }

        return os;
    }

    typedef SBuffer<float, DYN>    SBufferXf;
    typedef SBuffer<float, DYN, 2> SBufferX2f;
    typedef SBuffer<float, DYN, 3> SBufferX3f;
    typedef SBuffer<float, DYN, 4> SBufferX4f;

    typedef SBuffer<int32_t, DYN>    SBufferXi32;
    typedef SBuffer<int32_t, DYN, 2> SBufferX2i32;
    typedef SBuffer<int32_t, DYN, 3> SBufferX3i32;
    typedef SBuffer<int32_t, DYN, 4> SBufferX4i32;

    typedef SBuffer<int, DYN>    SBufferXi;
    typedef SBuffer<int, DYN, 2> SBufferX2i;
    typedef SBuffer<int, DYN, 3> SBufferX3i;
    typedef SBuffer<int, DYN, 4> SBufferX4i;

    typedef SBuffer<uint32_t, DYN>    SBufferXu32;
    typedef SBuffer<uint32_t, DYN, 2> SBufferX2u32;
    typedef SBuffer<uint32_t, DYN, 3> SBufferX3u32;
    typedef SBuffer<uint32_t, DYN, 4> SBufferX4u32;

    typedef SBuffer<unsigned int, DYN>    SBufferXu;
    typedef SBuffer<unsigned int, DYN, 2> SBufferX2u;
    typedef SBuffer<unsigned int, DYN, 3> SBufferX3u;
    typedef SBuffer<unsigned int, DYN, 4> SBufferX4u;
}


#endif  // CROSSLIGHT_CORE_SBUFFER_H_
