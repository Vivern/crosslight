#ifndef CROSSLIGHT_CORE_BUFFER_H_
#define CROSSLIGHT_CORE_BUFFER_H_


#include <memory>
#include <functional>
#include "type_helpers.hpp"
#include "MemoryKeeper.hpp"


namespace cl {
    const int MAX_BUFFER_ORDER = 4;

    class Buffer final {
    public:
        static std::shared_ptr<Buffer> createNew(int order, const int *shape, NPY_TYPES dataType);

        static std::shared_ptr<Buffer> createNew(
                const std::shared_ptr<MemoryKeeper> &memory,
                void *first,
                NPY_TYPES dType,
                int order,
                const int *shape,
                const int *strides);

        Buffer(const Buffer &) = delete;

        ~Buffer() = default;

        const std::shared_ptr<MemoryKeeper> &memory() const;

        NPY_TYPES dType() const;

        int order() const;

        const int *shape() const;

        int shape(int dim) const;

        const int *strides() const;

        int stride(int dim) const;

        void *first() const;

        void *first();

        const void *getVoid(int nIndices, const int *indices) const;

        void *getVoid(int nIndices, const int *indices);

        template<typename T>
        const T &get(int nIndices, const int *indices) const {
            assertLe(nIndices, order());
            assertEq(cTypeToNp<T>(), dType());

            uint8_t *result = first_;

            for (int ii=0; ii<nIndices; ++ii) {
                int index = indices[ii];
                assertGe(index, 0);
                assertLt(index, shape_[ii]);

                result += index * strides_[ii];
            }

            return *reinterpret_cast<T *>(result);
        }

        template<typename T>
        T &get(int nIndices, const int *indices) {
            const auto *constThis = this;

            return const_cast<T &>( constThis->get<T>(nIndices, indices) );
        }

        std::shared_ptr<Buffer> subBuffer(const int *firstIndex, const int *subShape) const;

    private:
        /**
         * Internal constructor. Use the `createNew` factory methods to instantiate new buffers.
         */
        Buffer(const std::shared_ptr<MemoryKeeper> &memory,
               void *first,
               NPY_TYPES dType,
               int order,
               const int *sizes,
               const int *strides);

        std::weak_ptr<Buffer> weakThis;
        const std::shared_ptr<MemoryKeeper> memory_;
        uint8_t * const first_;
        const NPY_TYPES dType_;
        const int order_;
        int shape_[MAX_BUFFER_ORDER];
        int strides_[MAX_BUFFER_ORDER];
    };
}


#endif  // CROSSLIGHT_CORE_BUFFER_H_
