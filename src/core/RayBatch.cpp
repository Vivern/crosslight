#include "RayBatch.hpp"
#include <fancy-assert>


namespace cl {
    RayBatch::RayBatch(const std::shared_ptr<Intersector> &intersector, int size)
        : intersector(intersector), size_(size) {

        assertGt(size, 0);
    }

    void RayBatch::initBuffers(
            const SBufferXu  &rayIdBuffer,
            const SBufferX3f &originBuffer,
            const SBufferX3f &dirBuffer,
            const SBufferXf  &lengthBuffer,
            const SBufferXu  &geometryIdBuffer,
            const SBufferXu  &primitiveIdBuffer) {

        this->rayIdBuffer = rayIdBuffer;

        this->originBuffer = originBuffer;
        this->dirBuffer = dirBuffer;
        this->lengthBuffer = lengthBuffer;

        this->geometryIdBuffer = geometryIdBuffer;
        this->primitiveIdBuffer = primitiveIdBuffer;
    }

    int RayBatch::size() const {
         return size_;
    }

    std::shared_ptr<Intersector> RayBatch::getIntersector() const {
        return intersector;
    }

    SBufferXu &RayBatch::getRayIdBuffer() {
        return rayIdBuffer;
    }

    SBufferX3f &RayBatch::getOriginBuffer() {
        return originBuffer;
    }

    SBufferX3f &RayBatch::getDirBuffer() {
        return dirBuffer;
    }

    SBufferXf &RayBatch::getLengthBuffer() {
        return lengthBuffer;
    }

    SBufferXu &RayBatch::getGeometryIdBuffer() {
        return geometryIdBuffer;
    }

    SBufferXu &RayBatch::getPrimitiveIdBuffer() {
        return primitiveIdBuffer;
    }
}
