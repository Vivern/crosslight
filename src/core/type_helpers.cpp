#include <fancy-assert>
#include "type_helpers.hpp"


namespace cl {

    type_info::type_info(size_t size, size_t alignment)
            : size(size), alignment(alignment) {}

    type_info::type_info(NPY_TYPES npy_type_num) {

        switch (npy_type_num) {
            case NPY_INT8:     *this = type_info::forType< npTypeToC<NPY_INT8   >::type >(); return;
            case NPY_INT16:    *this = type_info::forType< npTypeToC<NPY_INT16  >::type >(); return;
            case NPY_INT32:    *this = type_info::forType< npTypeToC<NPY_INT32  >::type >(); return;
            case NPY_INT64:    *this = type_info::forType< npTypeToC<NPY_INT64  >::type >(); return;
            case NPY_UINT8:    *this = type_info::forType< npTypeToC<NPY_UINT8  >::type >(); return;
            case NPY_UINT16:   *this = type_info::forType< npTypeToC<NPY_UINT16 >::type >(); return;
            case NPY_UINT32:   *this = type_info::forType< npTypeToC<NPY_UINT32 >::type >(); return;
            case NPY_UINT64:   *this = type_info::forType< npTypeToC<NPY_UINT64 >::type >(); return;
//            case NPY_FLOAT16:  *this = type_info::forType< npTypeToC<NPY_FLOAT16>::type >(); return;
            case NPY_FLOAT32:  *this = type_info::forType< npTypeToC<NPY_FLOAT32>::type >(); return;
            case NPY_FLOAT64:  *this = type_info::forType< npTypeToC<NPY_FLOAT64>::type >(); return;

            default:           assertNotReached();
        }
    }

    template<> NPY_TYPES cTypeToNp<int8_t  >()  { return NPY_INT8;    }
    template<> NPY_TYPES cTypeToNp<int16_t >()  { return NPY_INT16;   }
    template<> NPY_TYPES cTypeToNp<int32_t >()  { return NPY_INT32;   }
    template<> NPY_TYPES cTypeToNp<int64_t >()  { return NPY_INT64;   }
    template<> NPY_TYPES cTypeToNp<uint8_t >()  { return NPY_UINT8;   }
    template<> NPY_TYPES cTypeToNp<uint16_t>()  { return NPY_UINT16;  }
    template<> NPY_TYPES cTypeToNp<uint32_t>()  { return NPY_UINT32;  }
    template<> NPY_TYPES cTypeToNp<uint64_t>()  { return NPY_UINT64;  }
//    template<> NPY_TYPES cTypeToNp<half    >()  { return NPY_FLOAT16; }
    template<> NPY_TYPES cTypeToNp<float   >()  { return NPY_FLOAT32; }
    template<> NPY_TYPES cTypeToNp<double  >()  { return NPY_FLOAT64; }
}
