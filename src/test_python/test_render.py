import unittest
import numpy as np
import crosslight as cl


class TestRender(unittest.TestCase):
    def test_render(self):
        xres, yres = 16, 8

        # Set up the scene
        scene = cl.Scene()
        geom = cl.TriangleMesh.create_box(1)
        scene.add(geom)

        # Set up the intersector and ray batch
        intersector = cl.Intersector('embree')
        batch = intersector.create_batch(xres * yres)
        fscene = scene.freeze(intersector)

        # Initialize the rays
        lincoord = np.arange(0, xres * yres)
        xcoord = lincoord % xres
        ycoord = lincoord // xres

        cam1, cam2 = -1, 1

        # origins
        origins = np.stack(
            (
                cam1 + (xcoord / xres) * (cam2 - cam1),
                cam1 + (ycoord / yres) * (cam2 - cam1),
                np.full(yres * xres, -10),
            ),
            axis=1
        )

        np.copyto(batch.origins, origins)

        # Directions
        np.copyto(batch.directions, (0, 0, 1))

        # Lengths
        np.copyto(batch.lengths, 10000)

        # Intersect
        intersector.find_intersections(
            fscene,
            batch,
            'closest'
        )

        # Construct the result
        distances = np.reshape(batch.lengths, (yres, xres))

        # Verify it matches the expectations
        for yy in range(yres):
            for xx in range(xres):
                # Watch out, the image's y coordinate is flipped!
                should_hit = xx >= xres/2 and yy >= yres/2

                distance = distances[yy, xx]

                if should_hit:
                    self.assertAlmostEqual(distance, 10, delta=0.5)
                else:
                    self.assertAlmostEqual(distance, 10000, delta=0.5)


if __name__ == '__main__':
    unittest.main()
